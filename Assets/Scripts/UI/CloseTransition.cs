﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CloseTransition : MonoBehaviour
{
    private int sceneIndex;
    public int SceneIndex
    {
        set { sceneIndex = value; }
    }

    private void Update()
    {
        if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Finished"))
            SceneManager.LoadScene(sceneIndex);
    }
}
