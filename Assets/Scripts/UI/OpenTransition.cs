﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenTransition : MonoBehaviour
{
    private void Update()
    {
        if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Finished"))
            Destroy(this.gameObject);
    }
}
