﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergyText : MonoBehaviour
{
    private Text thisText;
    public Text otherText;
    private Animator anim;
    private bool flash;

    void Start()
    {
        thisText = GetComponent<Text>();
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (PlayerMovement.bondEnergy > 0 && PlayerMovement.energy == 0)
        {
            string t = PlayerMovement.bondEnergy.ToString("0.0");
            thisText.text = t;
            otherText.text = "<color=#00acc1>" + t + "</color>";
        }
        else
        {
            string t = PlayerMovement.energy.ToString("0.0");
            string t1 = (PlayerMovement.energy <= 50) ? "<color=#e91e63>" + t + "</color>" : t;
            thisText.text = (PlayerMovement.bondEnergy > 0) ? t + "+" + PlayerMovement.bondEnergy.ToString("0.0") : t;
            otherText.text = (PlayerMovement.bondEnergy > 0) ? t1 + "<color=#00acc1>+" + PlayerMovement.bondEnergy.ToString("0.0") + "</color>" : t1;
        }
    }

    public void Grow()
    {
        if (anim.GetCurrentAnimatorStateInfo(1).IsName("Default"))
            anim.SetTrigger("Grow");
    }

    public void Shrink()
    {
        anim.SetTrigger("Shrink");
    }

    public void Flash(bool f)
    {
        anim.SetBool("Flash", f);
    }
}
