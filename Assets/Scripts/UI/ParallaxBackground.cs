﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxBackground : MonoBehaviour
{
    private Vector3 lastCameraPos;

    private void Start()
    {
        lastCameraPos = Camera.main.transform.position;
    }

    private void Update()
    {
        Vector3 currentCameraPos = Camera.main.transform.position;
        transform.position += (currentCameraPos - lastCameraPos) * 0.7f;

        lastCameraPos = currentCameraPos;
    }
}
