﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraFollow : MonoBehaviour
{
    public Transform player;
    public Transform reticle;
    private bool shake;
    private float shakeT;
    private float shakeTimer = 0.2f;
    private float shakeFactor;
    private float holdT;
    public bool canShake;

    private void Update()
    {
        Vector3 between = reticle.position - player.position;   //Vector between player and reticle
        Vector3 playerVel = player.GetComponent<Rigidbody2D>().velocity;
        Vector3 follow = player.position + playerVel.normalized * 90 + between.normalized * (between.magnitude * 0.45f); //follow vector that is 0.45 of the way from the player to the reticle, and also partly in the direction of the player's velocity

        transform.position = Vector3.Lerp(transform.position, new Vector3(follow.x, follow.y, -10), 0.1f);

        if (shake && canShake)
        {
            if (shakeT < shakeTimer)
            {
                Vector3 randomPos = Random.insideUnitCircle;

                transform.position += randomPos * shakeFactor;
                shakeT += Time.deltaTime;
            }
            else
            {
                shakeT = 0;
                shake = false;
            }
        }

        if (holdT > 0)
        {
            Time.timeScale = 0;
            holdT -= Time.unscaledDeltaTime;
        }
        else
        {
            holdT = 0;
            Time.timeScale = 1;
        }
    }

    public void ScreenShake(float f)
    {
        if (!shake)
        {
            shake = true;
            shakeFactor = f;
        }
    }

    public void Hold(float t)
    {
        if (holdT == 0)
            holdT = t;
    }
}
