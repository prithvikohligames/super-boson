﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergyDrop : MonoBehaviour
{
    private Text thisText;
    public Text otherText;
    private double energy;
    public double Energy
    {
        set { energy = value; }
    }
    private Animator anim;

    private void Start()
    {
        thisText = GetComponent<Text>();
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        string t = "+" + energy.ToString();
        thisText.text = t;
        otherText.text = t;

        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Finished"))
            Destroy(gameObject);
    }
}
