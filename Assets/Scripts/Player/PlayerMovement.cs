﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public static float energy = 100.0f;
    public static float bondEnergy = 0.0f;
    public float energyDecay;
    private Rigidbody2D rb;
    public float speed;
    public Transform reticle;
    public ParticleSystem partSys;
    private Animator anim;
    public GameObject chargeSquare;
    private bool spawnedChargeSquare;
    private float collideT;
    public float CollideT
    {
        get { return collideT; }
        set { collideT = value; }
    }
    private float collideTimer = 1.32f;
    public float CollideTimer
    {
        get { return collideTimer; }
        set { collideTimer = value; }
    }
    private bool invincible;
    public bool Invincible
    {
        get { return invincible; }
    }
    private float invincibleT;
    private float invincibleTimer = 0.07f;
    public EnergyText energyText;
    public Animator screenRedFlash;
    private float previousBondEnergy;
    private AudioSource[] audioSources;
    public static bool annihilated;
    public GameObject annihilateExplosion;
    public GameObject fissionExplosion;
    public GameObject closeTransition;

    private void Start()
    {
        annihilated = false;
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        audioSources = GetComponents<AudioSource>();
    }

    private void Update()
    {
        if (!annihilated)
        {
            Quaternion rotateTo = Quaternion.LookRotation(Vector3.forward, reticle.position - transform.position);
            rb.MoveRotation(rotateTo.eulerAngles.z);   //Look towards reticle
            if (!reticle.GetComponent<Reticle>().Charged && collideT == 0 && StageManager.spawnStartT <= 0)
            {
                if (energy - energyDecay * Time.deltaTime > 0)
                    energy -= energyDecay * Time.deltaTime;
                else
                {
                    energy = 0;

                    if (previousBondEnergy == 0)
                        previousBondEnergy = bondEnergy;

                    if (bondEnergy - energyDecay * Time.deltaTime > 0)
                        bondEnergy -= energyDecay * Time.deltaTime;
                    else
                        bondEnergy = 0;
                }
            }

            if (energy == 0 && bondEnergy == 0)
            {
                Annihilate();
                audioSources[3].Play();
                Instantiate(annihilateExplosion, transform.position, Quaternion.identity);
            }
            else if (energy + bondEnergy >= 500)
            {
                Annihilate();
                audioSources[4].Play();
                Instantiate(fissionExplosion, transform.position, Quaternion.identity);
            }

        }
    }

    private void FixedUpdate()
    {
        if (!annihilated)
        {
            if (collideT == 0)  //If we're not colliding
            {
                if (!reticle.GetComponent<Reticle>().Charged)
                {
                    rb.velocity = transform.up * speed;
                    partSys.Play();

                    spawnedChargeSquare = false;
                }
                else    //If we're charged up  
                {
                    rb.velocity = Vector2.zero;
                    partSys.Stop();
                    anim.SetBool("flash", true);

                    if (!spawnedChargeSquare)
                    {
                        Quaternion rotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(1, 359)));
                        Instantiate(chargeSquare, transform.position, rotation, transform);
                        spawnedChargeSquare = true;

                        audioSources[1].Play();
                    }
                }

                if (invincible)
                {
                    if (invincibleT < invincibleTimer)
                        invincibleT += Time.deltaTime;
                    else
                    {
                        invincible = false;
                        invincibleT = 0;
                        anim.SetBool("flash", false);
                    }
                }
            }
            else    //If we're colliding
            {
                if (collideT < collideTimer)
                {
                    float t = collideT;
                    float v = 3000 * Mathf.Abs((3 * t - 1) * (t - 1.32f)) + 300;
                    rb.velocity = transform.up * v;
                    partSys.Play();

                    collideT += Time.deltaTime;
                }
                else
                {
                    collideT = 0;
                    invincible = true;
                }
            }
        }
        else
        {
            transform.localScale = Vector3.zero;
            if (anim.GetCurrentAnimatorStateInfo(0).IsName("Annihilated"))
            {
                anim.enabled = false;
                CloseTransition c = Instantiate(closeTransition, Camera.main.transform.position + new Vector3(0, 0, 10), Quaternion.identity, Camera.main.transform).GetComponent<CloseTransition>();
                c.SceneIndex = (energy + bondEnergy >= 500) ? 4 : 3;
            }
        }
    }

    public void GainEnergy(float e)
    {
        if (energy != 0)
            energy += e;
        else
        {
            if (bondEnergy != 0)
            {
                if (bondEnergy + e < previousBondEnergy)
                    bondEnergy += e;
                else
                {
                    energy += e - (previousBondEnergy - bondEnergy);
                    bondEnergy = previousBondEnergy;
                    previousBondEnergy = 0;
                }
            }
        }
        energyText.Grow();
    }
    
    public void LoseEnergy(float e)
    {
        if (energy - e > 0)
            energy -= e;
        else
        {
            if (bondEnergy - e > 0)
                bondEnergy -= e;
            else
                bondEnergy = 0;

            energy = 0;
        }
        energyText.Shrink();
        screenRedFlash.SetTrigger("Flash");

        invincible = true;
    }

    public void GainBondEnergy(float e)
    {
        bondEnergy += e;
        energyText.Grow();
    }

    public void LoseBondEnergy(float e)
    {
        if (bondEnergy - e > 0)
            bondEnergy -= e;
        else
            bondEnergy = 0;

        energyText.Shrink();
    }

    private void Annihilate()
    {
        annihilated = true;
        rb.velocity = Vector2.zero;
        GetComponent<BoxCollider2D>().enabled = false;
        Destroy(reticle.GetComponent<Reticle>());
        anim.SetTrigger("annihilate");
        audioSources[0].Stop();
    }
}
