﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public static double energy = 100.0;
    public float energyDecay;
    public float chargeEnergy;
    private Rigidbody2D rb;
    public float speed;
    public Transform reticle;
    public ParticleSystem partSys;
    private Animator anim;
    public GameObject chargeSquare;
    private bool spawnedChargeSquare;
    private float collideT;
    public float CollideT
    {
        get { return collideT; }
        set { collideT = value; }
    }
    private float collideTimer = 1.32f;
    public float CollideTimer
    {
        get { return collideTimer; }
        set { collideTimer = value; }
    }
    private bool invincible;
    public bool Invincible
    {
        get { return invincible; }
    }
    private float invincibleT;
    private float invincibleTimer = 0.03f;
    public EnergyText energyText;
    public Animator screenRedFlash;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        Quaternion rotateTo = Quaternion.LookRotation(Vector3.forward, reticle.position - transform.position);
        rb.MoveRotation(rotateTo.eulerAngles.z);   //Look towards reticle

        energy -= energyDecay * Time.deltaTime;
    }

    private void FixedUpdate()
    {
        if (collideT == 0)  //If we're not colliding
        {
            if (!reticle.GetComponent<Reticle>().Charged)
            {
                rb.velocity = transform.up * speed;
                partSys.Play();
                anim.SetBool("flash", false);

                spawnedChargeSquare = false;
            }
            else    //If we're charged up  
            {
                rb.velocity = Vector2.zero;
                partSys.Stop();
                anim.SetBool("flash", true);

                if (!spawnedChargeSquare)
                {
                    Quaternion rotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(1, 359)));
                    Instantiate(chargeSquare, transform.position, rotation, transform);
                    spawnedChargeSquare = true;
                }
            }

            if (invincible)
            {
                if (invincibleT < invincibleTimer)
                    invincibleT += Time.deltaTime;
                else
                {
                    invincible = false;
                    invincibleT = 0;
                }
            }
        }
        else    //If we're colliding
        {
            if (collideT < collideTimer)
            {
                float t = collideT;
                float v = 3000 * Mathf.Abs((3 * t - 1) * (t - 1.32f)) + 300;
                rb.velocity = transform.up * v;

                partSys.Play();
                anim.SetBool("flash", false);

                collideT += Time.deltaTime;
            }
            else
            {
                collideT = 0;
                invincible = true;
            }
        }
    }

    public void GainEnergy(float e)
    {
        energy += e;
        energyText.Grow();
    }
    
    public void LoseEnergy(float e)
    {
        energy -= e;
        energyText.Shrink();
        screenRedFlash.SetTrigger("Flash");

        invincible = true;
    }
}
