﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyPlayer : MonoBehaviour
{
    private void Update()
    {
        transform.position = (Vector3)Random.insideUnitCircle * 20;
    }
}
