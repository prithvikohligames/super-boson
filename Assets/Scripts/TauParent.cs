﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TauParent : ParentParticle
{
    private float angle;
    public float orbitSpeed;
    public float rotateSpeed;
    public float decayT;
    public float radius;
    public GameObject exclamationMark;
    private bool spawnedExclamationMark;

    private void Update()
    {
        angle = (angle < 360) ? angle + orbitSpeed * Time.deltaTime : 0;

        Vector2 p = player.position;
        Vector3 moveTo = p + new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)) * radius;

        transform.position += (moveTo - transform.position).normalized * speed * Time.deltaTime;

        Quaternion rotateTo = Quaternion.LookRotation(Vector3.forward, player.position - transform.position);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, rotateTo, rotateSpeed * Time.deltaTime);

        if (decayT > 0)
            decayT -= Time.deltaTime;
        else
            Annihilate();

        if (decayT < 1 && !spawnedExclamationMark)
        {
            ExclamationMark e = Instantiate(exclamationMark, transform.position, Quaternion.identity, worldCanvas).GetComponent<ExclamationMark>();
            e.Follow = transform;
            spawnedExclamationMark = true;
        }

        if (Vector2.Distance(transform.position, player.position) > 550)
            transform.position = (Vector2)player.position + Vector2.ClampMagnitude(transform.position - player.position, 550);
    }
}
