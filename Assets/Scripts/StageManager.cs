﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StageManager : MonoBehaviour
{
    public int maxTotalParticles;
    public int maxEachParticles;
    private int totalParticles;
    public static int[] particleNumbers;
    public GameObject[] particles;
    public Transform[] spawnLocations;
    public GameObject closeTransition;
    public static float spawnStartT;

    private void Start()
    {
        particleNumbers = new int[particles.Length];
        PlayerMovement.energy = 100;
        PlayerMovement.bondEnergy = 0;
        BondableParticle.bonded = false;
        spawnStartT = 1.5f;
        AudioListener.volume = 1;
    }

    private void Update()
    {
        if (spawnStartT <= 0)
        {
            int t = 0;
            for (int x = 0; x < particleNumbers.Length; x++)
            {
                t += particleNumbers[x];
            }
            totalParticles = t;

            if (totalParticles < maxTotalParticles)
            {
                int p = Random.Range(0, particles.Length);
                if (particleNumbers[p] < maxEachParticles)
                {
                    int s = Random.Range(0, spawnLocations.Length);
                    Instantiate(particles[p], spawnLocations[s].position, Quaternion.identity);
                    particleNumbers[p]++;
                }
            }
        }
        else
            spawnStartT -= Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.Escape) && SceneManager.GetActiveScene().buildIndex != 0)
        {
            CloseTransition c = Instantiate(closeTransition, new Vector3(transform.position.x, transform.position.y, 10), Quaternion.identity, transform).GetComponent<CloseTransition>();
            c.SceneIndex = 0;
        }
    }
}
