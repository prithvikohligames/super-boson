﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dialogue : MonoBehaviour
{
    public string[] lines;
    protected int lineIndex;
    private char[] chars;
    private int charIndex;
    public Text txt;
    public Text txtBlack;
    private float typeT;
    private float typeTimer = 0.04f;
    private bool paused = false;
    public GameObject closeTransition;
    public int sceneIndex;

    private void Start()
    {
        Cursor.visible = false;
        lineIndex = 0;
        chars = lines[lineIndex].ToCharArray();
        typeT = typeTimer;
    }

    protected virtual void Update()
    {
        if (!paused)
        {
            if (Input.anyKeyDown)
            {
                string t = lines[lineIndex] + "\n\n[PRESS ANY KEY]";
                txt.text = t;
                txtBlack.text = t;

                charIndex = chars.Length;
                typeT = 0;
                paused = true;
            }

            typeT += Time.deltaTime;
            if (typeT >= typeTimer)
            {
                char c = chars[charIndex];
                txt.text += c;
                txtBlack.text += c;

                if (charIndex < chars.Length - 1)
                    charIndex++;
                else
                {
                    paused = true;

                    string t = "\n\n[PRESS ANY KEY]";
                    txt.text += t;
                    txtBlack.text += t;

                }

                typeT = 0;
            }
        }
        else
        {
            if (Input.anyKeyDown)
            {
                if (lineIndex == lines.Length - 1)
                {
                    CloseTransition t = Instantiate(closeTransition, Vector3.zero, Quaternion.identity).GetComponent<CloseTransition>();
                    t.SceneIndex = sceneIndex;
                }
                else
                {
                    txt.text = "";
                    txtBlack.text = "";

                    lineIndex++;

                    chars = lines[lineIndex].ToCharArray();
                    charIndex = 0;

                    paused = false;
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            CloseTransition t = Instantiate(closeTransition, Vector3.zero, Quaternion.identity).GetComponent<CloseTransition>();
            t.SceneIndex = 0;
        }
    }
}
