﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergyText : MonoBehaviour
{
    private Text thisText;
    public Text otherText;
    private Animator anim;
    private bool flash;

    void Start()
    {
        thisText = GetComponent<Text>();
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        string t = PlayerMovement.energy.ToString("0.0");

        thisText.text = t;
        otherText.text = t;
    }

    public void Grow()
    {
        if (anim.GetCurrentAnimatorStateInfo(1).IsName("Default"))
            anim.SetTrigger("Grow");
    }

    public void Shrink()
    {
        anim.SetTrigger("Shrink");
    }

    public void Flash(bool f)
    {
        anim.SetBool("Flash", f);
    }
}
