﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChildParticle : Particle
{
    public Sprite newSprite;
    protected Animator anim;
    public GameObject annihilateSquare;
    public float annihilateT;
    private float annihilateTimer;
    protected bool annihilated;

    protected virtual void Start()
    {
        GetComponent<SpriteRenderer>().sprite = newSprite;
        GetComponent<BoxCollider2D>().enabled = true;
        anim = GetComponent<Animator>();

        anim.enabled = false;   //Disable animator so we can change scale
        transform.localScale = new Vector3(1.5f, 1.5f, 1);

        annihilateTimer = annihilateT;
    }

    protected virtual void Update()
    {
        if (annihilateT <= 0)
            Annihilate();
        else
            annihilateT -= Time.deltaTime;
    }

    private void Annihilate()
    {
        if (!annihilated)
        {
            GetComponent<BoxCollider2D>().enabled = false;
            anim.enabled = true;
            anim.SetTrigger("Annihilate");

            Quaternion rotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(1, 359)));
            Instantiate(annihilateSquare, transform.position, rotation, transform);

            annihilated = true;
        }

        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Annihilated"))
            Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" && annihilateT < annihilateTimer - 0.1f)
        {
            CameraFollow cam = Camera.main.GetComponent<CameraFollow>();
            PlayerMovement p = collision.GetComponent<PlayerMovement>();
            if (p.CollideT > 0 && p.CollideT < 1.25f)
            {
                p.GainEnergy(energyGain);
                EnergyDrop e = Instantiate(energyDrop, transform.position, Quaternion.identity, worldCanvas).GetComponent<EnergyDrop>();
                e.Energy = energyGain;

                cam.ScreenShake(40);
                cam.Hold(0.11f);

                annihilateT = 0;
            }
            else
            {
                if (!p.Invincible)
                {
                    p.LoseEnergy(energyLoss);
                    cam.ScreenShake(40);
                    annihilateT = 0;
                }
            }
        }
    }
}
