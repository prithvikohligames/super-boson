﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reticle : MonoBehaviour
{
    public Transform player;
    private Vector3 offset; //Offset of reticle from player position
    public float speed;
    public Transform[] reticleCorners = new Transform[4];
    private Vector3[] cornersOriginalLocalPos = new Vector3[4];
    private Animator anim;
    private bool charged;
    public bool Charged
    {
        get { return charged; }
    }
    private float chargeT;
    private float chargeTimer = 0.05f;
    public GameObject collideSquare;
    public float chargeEnergyDecay;
    public EnergyText energyText;

    private void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        offset = transform.position - player.position;

        for (int x = 0; x < 4; x++)
        {
            cornersOriginalLocalPos[x] = reticleCorners[x].localPosition;
        }

        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        offset = Vector3.ClampMagnitude(offset, 160);
        transform.position = player.position + offset;

        offset.x += Input.GetAxis("Mouse X") * speed * Time.deltaTime;
        offset.y += Input.GetAxis("Mouse Y") * speed * Time.deltaTime;

        transform.rotation = Quaternion.LookRotation(Vector3.forward, player.position - transform.position);

        if (Input.GetMouseButton(0) && player.GetComponent<PlayerMovement>().CollideT == 0)
        {
            foreach (Transform t in reticleCorners)
            {
                t.localPosition = Vector3.Lerp(t.localPosition, new Vector3(0, 0, 0), 0.2f);
            }

            if (chargeT < chargeTimer)
                chargeT += Time.deltaTime;
            else
            {
                if (!charged)
                {
                    charged = true;
                    anim.SetBool("flash", true);
                    PlayerMovement.energy -= chargeEnergyDecay;

                    energyText.Flash(true);
                    energyText.Shrink();
                }
            }

            transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(3, 3, 1), 0.1f);
        }
        else
        {
            for (int x = 0; x < 4; x++)
            {
                Transform c = reticleCorners[x];
                c.localPosition = Vector3.Lerp(c.localPosition, cornersOriginalLocalPos[x], 0.2f);
            }

            transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(1.5f, 1.5f, 1), 0.1f);

            if (charged)
            {
                player.GetComponent<PlayerMovement>().CollideT = 1;
                charged = false;

                Quaternion rotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(1, 359)));
                Instantiate(collideSquare, player.position, rotation, player);

                Camera.main.GetComponent<CameraFollow>().ScreenShake(30);

                energyText.Flash(false);
            }

            chargeT = 0;

            anim.SetBool("flash", false);
        }
    }
}
