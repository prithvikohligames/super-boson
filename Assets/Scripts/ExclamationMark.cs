﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExclamationMark : MonoBehaviour
{
    private Transform follow;
    public Transform Follow
    {
        set { follow = value; }
    }

    private void Update()
    {
        transform.position = follow.position + follow.right * 32;

        if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Finished") || follow == null)
            Destroy(this.gameObject);
    }
}
