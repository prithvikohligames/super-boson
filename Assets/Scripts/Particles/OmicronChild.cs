﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OmicronChild : ChildParticle
{
    public float startAngle;
    public float rotateSpeed;

    protected override void Start()
    {
        base.Start();
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, startAngle));
    }

    protected override void Update()
    {
        base.Update();
        if (!annihilated)
        {
            transform.position += transform.up * speed * Time.deltaTime;

            Vector3 r = transform.rotation.eulerAngles;
            r.z += rotateSpeed * Time.deltaTime;
            Quaternion rotation = Quaternion.Euler(r);
            transform.rotation = rotation;
        }
    }
}
