﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParentParticle : Particle
{
    public Particle[] subParticles;
    public Bond[] unstableBonds;
    public GameObject explosion;
    protected Rigidbody2D rb;
    public int particleIndex;

    protected override void Start()
    {
        base.Start();
        rb = GetComponent<Rigidbody2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            PlayerMovement p = player.GetComponent<PlayerMovement>();
            if (p.CollideT > 0 && p.CollideT < 1.25f)
            {
                p.GainEnergy(energyGain);
                EnergyDrop e = Instantiate(energyDrop, transform.position, Quaternion.identity, worldCanvas).GetComponent<EnergyDrop>();
                e.Energy = energyGain;

                Camera.main.GetComponent<CameraFollow>().Hold(0.05f);
                audioSources[0].Play();

                Annihilate();
            }
            else
            {
                if (!p.Invincible)
                {
                    p.LoseEnergy(energyLoss);
                    audioSources[1].Play();
                    Annihilate();
                }
            }
        }
        else if (collision.tag == "BondedParticle")
        {
            PlayerMovement p = player.GetComponent<PlayerMovement>();
            p.GainEnergy(energyGain);
            EnergyDrop e = Instantiate(energyDrop, transform.position, Quaternion.identity, worldCanvas).GetComponent<EnergyDrop>();
            e.Energy = energyGain;
            audioSources[0].Play();

            Annihilate();
        }
    }

    protected virtual void Annihilate()
    {
        Camera.main.GetComponent<CameraFollow>().ScreenShake(40);
        Quaternion rotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(1, 359)));
        Instantiate(explosion, transform.position, rotation);

        foreach (Bond b in unstableBonds)
        {
            b.Break();
        }

        foreach (Particle p in subParticles)
        {
            p.enabled = true;
        }

        if (StageManager.particleNumbers[particleIndex] > 0)
            StageManager.particleNumbers[particleIndex]--;

        Destroy(rb);
        Destroy(GetComponent<BoxCollider2D>());
        transform.DetachChildren();
        Destroy(gameObject, 1);
        Destroy(this);
    }
}
