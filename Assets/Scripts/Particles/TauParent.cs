﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TauParent : ParentParticle
{
    private float angle;
    private float orbitSpeed;
    public float rotateSpeed;
    public float decayT;
    public float radius;
    public GameObject decayChargeSquare;
    private bool spawnedDecayChargeSquare;
    private GameObject instantiatedDecayChargeSquare;

    protected override void Start()
    {
        base.Start();
        decayT = Random.Range(decayT - 0.5f, decayT + 0.5f);
        orbitSpeed = (Random.Range(-1, 2) >= 0) ? 1 : -1; 
    }

    private void Update()
    {
        angle = (angle < 360) ? angle + orbitSpeed * Time.deltaTime : 0;

        Vector2 p = player.position;
        Vector3 moveTo = p + new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)) * radius;

        transform.position += (moveTo - transform.position).normalized * speed * Time.deltaTime;

        Quaternion rotateTo = Quaternion.LookRotation(Vector3.forward, player.position - transform.position);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, rotateTo, rotateSpeed * Time.deltaTime);

        if (decayT > 0)
            decayT -= Time.deltaTime;
        else
        {
            if (!PlayerMovement.annihilated)
            {
                audioSources[2].Play();
                Annihilate();
            }
        }

        if (decayT < 1.5f && !spawnedDecayChargeSquare)
        {
            instantiatedDecayChargeSquare = Instantiate(decayChargeSquare, transform.position + transform.right * 0.3f + transform.up * -16.7f, Quaternion.identity, transform);
            spawnedDecayChargeSquare = true;
        }

        if (Vector2.Distance(transform.position, player.position) > 500)
            transform.position = (Vector2)player.position + Vector2.ClampMagnitude(transform.position - player.position, 500);
    }

    protected override void Annihilate()
    {
        if (instantiatedDecayChargeSquare != null)
            Destroy(instantiatedDecayChargeSquare);
        base.Annihilate();
    }
}
