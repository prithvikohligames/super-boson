﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Particle : MonoBehaviour
{
    protected Transform player;
    public float speed;
    public float energyGain;
    public float energyLoss;
    public GameObject energyDrop;
    protected Transform worldCanvas;
    protected AudioSource[] audioSources;

    protected virtual void Start()
    {
        audioSources = GetComponents<AudioSource>();
        player = GameObject.Find("Player").transform;
        worldCanvas = GameObject.Find("WorldCanvas").transform;
    }
}
