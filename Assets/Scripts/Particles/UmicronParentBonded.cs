﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UmicronParentBonded : BondedParticle
{
    public Bond bond;
    public Bond otherBond;
    private Vector3 unitVel;
    public float rotateSpeed;
    public float annihilateT;
    private Animator anim;
    public GameObject annihilateSquare;
    private bool annihilated;

    protected override void Start()
    {
        base.Start();
        transform.localPosition = new Vector3(0, 26);
        transform.localRotation = Quaternion.Euler(0, 0, 180);
        anim = GetComponent<Animator>();
    }

    protected override void Update()
    {
        base.Update();
        if (decayed)
        {
            transform.position += unitVel * speed * Time.deltaTime;
            transform.rotation = Quaternion.Euler(0, 0, transform.eulerAngles.z + rotateSpeed * Time.deltaTime);

            if (annihilateT > 0)
                annihilateT -= Time.deltaTime;
            else if (!annihilated)
            {
                GetComponent<BoxCollider2D>().enabled = false;
                anim.SetTrigger("Annihilate");
                Quaternion rotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(1, 359)));
                Instantiate(annihilateSquare, transform.position, rotation, transform);
                annihilated = true;
            }

            if (anim.GetCurrentAnimatorStateInfo(0).IsName("Annihilated") && annihilated)
                Destroy(gameObject);
        }
    }

    protected override void Decay()
    {
        base.Decay();
        bond.Break();
        otherBond.Break();
        unitVel = -transform.up;
    }
}
