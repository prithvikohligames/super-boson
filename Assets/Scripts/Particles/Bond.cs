﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bond : MonoBehaviour
{
    private Animator anim;
    private bool broken;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        if (broken)
        {
            if (anim.GetCurrentAnimatorStateInfo(0).IsName("Broken"))
                Destroy(gameObject);
        }
    }

    public void Break()
    {
        anim.SetTrigger("break");
        broken = true;
    }
}
