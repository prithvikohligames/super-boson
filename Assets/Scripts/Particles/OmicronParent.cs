﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OmicronParent : ParentParticle
{
    public float rotateSpeed;

    private void Update()
    {
        Quaternion rotateTo = Quaternion.LookRotation(Vector3.forward, player.position - transform.position);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, rotateTo, rotateSpeed * Time.deltaTime);

        transform.position += transform.up * speed * Time.deltaTime;

        if (Vector2.Distance(transform.position, player.position) > 500)
            transform.position = (Vector2)player.position + Vector2.ClampMagnitude(transform.position - player.position, 500);
    }
}
