﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BondableParticle : Particle
{
    public GameObject bondedParticle;
    public GameObject bondedExplosion;
    public SpriteRenderer sr;
    public SpriteRenderer otherSr;
    public Sprite newSprite;
    private Sprite oldSprite;
    private float t;
    private bool canCollide;
    public static bool bonded;

    protected override void Start()
    {
        base.Start();
        GetComponent<BoxCollider2D>().enabled = true;
        transform.localScale = new Vector3(1.25f, 1.25f, 1);
        oldSprite = sr.sprite;
    }

    protected virtual void Update()
    {
        if (t < 0.1f)
            t += Time.deltaTime;
        else
            canCollide = true;

        if (!bonded)
        {
            if (sr.sprite != newSprite)
            {
                sr.sprite = newSprite;
                otherSr.sprite = newSprite;
                GetComponent<BoxCollider2D>().enabled = true;
            }
        }
        else
        {
            if (sr.sprite != oldSprite)
            {
                sr.sprite = oldSprite;
                otherSr.sprite = oldSprite;
                GetComponent<BoxCollider2D>().enabled = false;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" && canCollide && !bonded)
        {
            bonded = true;
            Camera.main.GetComponent<CameraFollow>().Hold(0.05f);
            Camera.main.GetComponent<CameraFollow>().ScreenShake(30);
            Quaternion rotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(1, 359)));
            Instantiate(bondedExplosion, transform.position, rotation);

            Instantiate(bondedParticle, player.position, Quaternion.identity, player);
            Destroy(this.gameObject);
        }
    }
}
