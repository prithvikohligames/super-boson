﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuonChild : ChildParticle
{
    public float dir;   //-ve or +ve for different directions
    private Vector3 unitVel;
    private bool swappedVel;

    protected override void Start()
    {
        base.Start();
        unitVel = transform.right * dir;
    }

    protected override void Update()
    {
        base.Update();
        transform.rotation = Quaternion.LookRotation(Vector3.forward, unitVel);
        transform.position += unitVel * speed * Time.deltaTime;

        if (annihilateT < annihilateTimer / 2 && !swappedVel)
        {
            unitVel = -unitVel;
            swappedVel = true;
        }
    }
}
