﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TauChild : ChildParticle
{
    public float rotateSpeed;
    private float initialRot;
    private float rotateTo;

    protected override void Start()
    {
        base.Start();
        initialRot = transform.eulerAngles.z;
        rotateTo = initialRot;
    }

    protected override void Update()
    {
        base.Update();
        transform.position += transform.up * speed * Time.deltaTime;

        rotateTo += rotateSpeed * Time.deltaTime;
        transform.rotation = Quaternion.Euler(0, 0, rotateTo);

        if ((rotateSpeed > 0 && rotateTo > initialRot + 90) || (rotateSpeed < 0 && rotateTo < initialRot - 90))
            rotateSpeed = -rotateSpeed;
    }
}
