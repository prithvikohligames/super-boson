﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BondedParticle : MonoBehaviour
{
    protected Transform player;
    public Transform Player
    {
        get { return player; }
    }
    public float bondEnergy;
    public float decayBondEnergyLoss;
    protected bool decayed;
    public float speed;
    public GameObject decayExplosion;

    protected virtual void Start()
    {
        player = transform.parent;
        player.GetComponent<PlayerMovement>().GainBondEnergy(bondEnergy);
    }

    protected virtual void Update()
    {
        PlayerMovement p = player.GetComponent<PlayerMovement>();
        if (Input.GetMouseButtonDown(1) && !decayed && p.CollideT == 0 && !p.reticle.GetComponent<Reticle>().Charged && PlayerMovement.energy != 0)
            Decay();
    }

    protected virtual void Decay()
    {
        decayed = true;
        Camera.main.GetComponent<CameraFollow>().ScreenShake(30);
        Quaternion rotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(1, 359)));
        Instantiate(decayExplosion, transform.position, rotation, transform);
        player.GetComponent<PlayerMovement>().LoseBondEnergy(decayBondEnergyLoss);

        transform.parent = null;
        GetComponent<BoxCollider2D>().enabled = true;

        Rigidbody2D rb = gameObject.AddComponent<Rigidbody2D>();
        rb.gravityScale = 0;
        rb.collisionDetectionMode = CollisionDetectionMode2D.Continuous;
        rb.interpolation = RigidbodyInterpolation2D.Interpolate;

        transform.localScale = new Vector3(1.25f, 1.25f, 1);

        BondableParticle.bonded = false;
    }
}
