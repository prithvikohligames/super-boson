﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UmicronParentBondable : BondableParticle
{
    private float angle;
    public float orbitSpeed;
    public float rotateSpeed;
    public float radius;
    public float annihilateT;
    private Animator anim;
    public GameObject annihilateSquare;
    private bool annihilated;

    protected override void Start()
    {
        anim = GetComponent<Animator>();
        anim.enabled = false;   //Disable animator so we can change scale
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
        angle = (angle < 360) ? angle + orbitSpeed * Time.deltaTime : 0;

        Vector2 p = player.position;
        Vector3 moveTo = p + new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)) * radius;

        transform.position += (moveTo - transform.position).normalized * speed * Time.deltaTime;
        transform.rotation = Quaternion.Euler(0, 0, transform.eulerAngles.z + rotateSpeed * Time.deltaTime);

        if (Vector2.Distance(transform.position, player.position) > 500)
            transform.position = (Vector2)player.position + Vector2.ClampMagnitude(transform.position - player.position, 500);

        if (annihilateT > 0)
            annihilateT -= Time.deltaTime;
        else if (!annihilated)
        {
            anim.enabled = true;
            anim.SetTrigger("Annihilate");
            Quaternion rotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(1, 359)));
            Instantiate(annihilateSquare, transform.position, rotation, transform);
            GetComponent<BoxCollider2D>().enabled = false;
            annihilated = true;
        }

        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Annihilated") && annihilated)
            Destroy(gameObject);
    }
}
