﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleScreen : MonoBehaviour
{
    public GameObject closeTransition;

    private void Start()
    {
        Cursor.visible = false;
        AudioListener.volume = 0;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
        else if (Input.anyKey)
        {
            CloseTransition c = Instantiate(closeTransition, Vector3.zero, Quaternion.identity).GetComponent<CloseTransition>();
            c.SceneIndex = 1;
            Destroy(this);
        }
    }
}
