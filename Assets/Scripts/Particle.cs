﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Particle : MonoBehaviour
{
    public Transform player;
    public float speed;
    public float energyGain;
    public float energyLoss;
    public GameObject energyDrop;
    public Transform worldCanvas;
}
