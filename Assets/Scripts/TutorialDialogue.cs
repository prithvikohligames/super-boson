﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialDialogue : Dialogue
{
    public Transform[] tutorialImages;

    protected override void Update()
    {
        base.Update();
        for (int x = 0; x < tutorialImages.Length; x++)
        {
            Transform t = tutorialImages[x];
            if (t != null)
            {
                Vector3 s = (x == lineIndex) ? new Vector3(4, 4, 1) : Vector3.zero;
                t.localScale = Vector3.Lerp(t.localScale, s, 0.2f);
            }
        }

        if (lineIndex == 4)
        {
            if (PlayerMovement.energy - 15 * Time.deltaTime > 0)
                PlayerMovement.energy -= 15 * Time.deltaTime;
            else
                PlayerMovement.energy = 0;
        }
    }
}
